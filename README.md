# Recorrido-Test

## Información
- Ruby 2.3.1
- Rails 5.1.4
- Postgres

## Instalación
```
$ bundle install
$ bundle exec figaro install
```
## Configuración BD Local

Crear BD Postgres:
```
$ createdb -Otunombredeusuario -Eutf8 recorrido-dev
```

Configurar `config/application.yml` añadiendo:
```
development:
  DBNAME: recorrido-dev
  DBUSER: tunombreusuario
  DBPASS: tupass

test:
  DBNAME: recorrido-dev
  DBUSER: tunombreusuario
  DBPASS: tupass
```

## Funcionamiento
```
$ rails s # Ejecuta el servidor
$ rake jobs:work # Ejecuta el funcionamiento del jobs

$ rake db:seed # Resetea La app
```