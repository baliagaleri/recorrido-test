class CreateBuses < ActiveRecord::Migration[5.1]
  def change
    create_table :buses do |t|
      t.string :internal_name
      t.string :official_name
      t.boolean :bookable
      t.string :phone
      t.boolean :allows_e_ticketing
      t.decimal :average_rating
      t.string :promotion_text
      t.string :change_date_policy

      t.timestamps
    end
  end
end
