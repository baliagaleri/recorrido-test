class AddAttachmentImageToBuses < ActiveRecord::Migration[5.1]
  def self.up
    change_table :buses do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :buses, :image
  end
end
