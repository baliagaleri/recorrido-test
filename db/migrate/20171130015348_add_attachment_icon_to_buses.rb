class AddAttachmentIconToBuses < ActiveRecord::Migration[5.1]
  def self.up
    change_table :buses do |t|
      t.attachment :icon
    end
  end

  def self.down
    remove_attachment :buses, :icon
  end
end
