class AddDescriptionToBus < ActiveRecord::Migration[5.1]
  def change
    add_column :buses, :description, :string
  end
end
