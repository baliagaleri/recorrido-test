class CreateRatings < ActiveRecord::Migration[5.1]
  def change
    create_table :ratings do |t|
      t.string :comment
      t.decimal :rating_total
      t.references :bus, foreign_key: true

      t.timestamps
    end
  end
end
