Rails.application.routes.draw do
  get 'buses/index'
  get 'buses/order_filter/:filter_option' => 'buses#order_filter', :as => 'buses_order_filter'

  get 'buses/show/:bus_id' => 'buses#show', :as => 'buses_show'
  get 'buses/edit/:bus_id' => 'buses#edit', :as => 'buses_edit'
  post 'buses/update/:bus_id' => 'buses#update', :as => 'buses_update'

  get 'buses/ratings/:bus_id' => 'buses#ratings', :as => 'buses_ratings'
  post 'buses/create_rating/:bus_id' => 'buses#create_rating', :as => 'buses_create_rating'
  get 'buses/get_bus_operators'

  root 'buses#index'
end
