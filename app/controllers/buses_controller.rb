class BusesController < ApplicationController

  before_action :set_bus, :only => [:show, :edit, :update, :ratings, :create_rating]

  def index
    begin
      filter_option = session[:filter_option]
    rescue
      filter_option = nil
    end
    Bus.delay(queue: 'bus', priority: 20).deliver
    @buses = Bus.filter(filter_option).paginate(:page => params[:page], :per_page => 20)
  end

  def order_filter
    session.delete(:filter_option)
    session[:filter_option] = params[:filter_option]
    redirect_to buses_index_path
  end

  def show
    @rating = Rating.new
  end

  def edit
  end

  def update
    if @bus.update(bus_params)

    else

    end
    redirect_to buses_show_path(@bus)
  end

  def ratings

  end

  def create_rating
    rating = @bus.ratings.new(rating_params)
    if rating.save

    end
    redirect_to buses_show_path(@bus)
  end

  # Obtiene de manera directa los buses, se reemplazó por un background job en el modelo para que no tarde tanto.
  def get_bus_operators
    response = HTTParty.get('https://www.recorrido.cl/api/v2/es/bus_operators.json')
    response.parsed_response['bus_operators'].each do |bus_operator|
      unless Bus.only_ids.include? (bus_operator['id'])
        icon = URI.parse(bus_operator['icons']['bigger'])
        image = URI.parse(bus_operator['images']['ios3x'])
        Bus.create(
          :id => bus_operator['id'],
          :internal_name => bus_operator['internal_name'],
          :official_name => bus_operator['official_name'],
          :bookable => bus_operator['bookable'],
          :phone => bus_operator['phone'],
          :allows_e_ticketing => bus_operator['allows_e_ticketing'],
          :average_rating => bus_operator['average_rating'],
          :promotion_text => bus_operator['promotion_text'],
          :change_date_policy => bus_operator['change_date_policy'],
          :icon => icon,
          :image => image
        )
      end
    end
  end

  private

  def set_bus
    @bus = Bus.find(params[:bus_id])
  end

  def bus_params
    params.require(:bus).permit(:description)
  end

  def rating_params
    params.require(:rating).permit(:comment, :rating_total)
  end
end
