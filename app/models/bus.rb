class Bus < ApplicationRecord
  has_many :ratings

  has_attached_file :icon, :styles => {:bigger => '300x122', :medium => '300x122', :thumb => '60x25'}
  has_attached_file :image, :styles => {:mdpi => '320x165', :hdpi => '480x248', :xhdpi => '640x331', :ios2x => '750x338', :ios3x => '1080x558'}

  validates_attachment_content_type :icon, content_type: /\Aimage\/.*\z/
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  def self.filter(filter_option)
    buses = self
    if filter_option.eql?('1')
      buses = self.group('buses.id').select('buses.id, buses.official_name, buses.icon_file_name, buses.icon_content_type, buses.icon_file_size, buses.icon_updated_at,
                        (buses.average_rating + (select COALESCE(sum(rating_total),0) from ratings where ratings.bus_id = buses.id)) / (CASE WHEN (select COALESCE(sum(rating_total),0) from ratings where ratings.bus_id = buses.id) = 0 THEN 1 ELSE 2 END) as rating_value')
                  .order('buses.official_name')
    elsif filter_option.eql?('2')
      buses = self.group('buses.id').select('buses.id, buses.official_name, buses.icon_file_name, buses.icon_content_type, buses.icon_file_size, buses.icon_updated_at,
                        (buses.average_rating + (select COALESCE(sum(rating_total),0) from ratings where ratings.bus_id = buses.id)) / (CASE WHEN (select COALESCE(sum(rating_total),0) from ratings where ratings.bus_id = buses.id) = 0 THEN 1 ELSE 2 END) as rating_value')
                  .order('rating_value DESC NULLS LAST')
    end
    buses
  end

  def self.only_ids
    self.ids
  end

  def self.deliver
    response = HTTParty.get('https://www.recorrido.cl/api/v2/es/bus_operators.json')
    response.parsed_response['bus_operators'].each do |bus_operator|
      unless Bus.only_ids.include? (bus_operator['id'])
        icon = URI.parse(bus_operator['icons']['bigger'])
        image = URI.parse(bus_operator['images']['ios3x'])
        Bus.create(
          :id => bus_operator['id'],
          :internal_name => bus_operator['internal_name'],
          :official_name => bus_operator['official_name'],
          :bookable => bus_operator['bookable'],
          :phone => bus_operator['phone'],
          :allows_e_ticketing => bus_operator['allows_e_ticketing'],
          :average_rating => bus_operator['average_rating'],
          :promotion_text => bus_operator['promotion_text'],
          :change_date_policy => bus_operator['change_date_policy'],
          :icon => icon,
          :image => image
        )
      end
    end
  end

  def calculate_rating
    average_rating = self.average_rating
    ratings = self.ratings.average(:rating_total)
    avg = (average_rating.zero? || average_rating.blank? || ratings.blank?) ? 1 : 2
    ((average_rating.blank? ? 0.0 : average_rating) + (ratings.blank? ? 0.0 : ratings)) / avg
  end
end
